from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from productos import urls as url_products
from usuarios import urls as url_users
from orden import urls as url_order
from api import urls as url_api
#from productos import urls as url_cart

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(url_products)),
    path('', include(url_users)),
    path('', include(url_order)),
    #path('', include(url_cart)),
    #path('/', admin.site.urls),
   path('api/', include(url_api)),
    


]

if settings.DEBUG: # new
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)