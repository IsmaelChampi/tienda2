#Models de usuarios
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
# Create your models here.


class Perfil(AbstractUser):
    """ Modelo de perfil de usuario """
    USERNAME_FIELD = 'username'

    edad = models.IntegerField(verbose_name=_('Edad'), blank=True, null=True)
    telefono = models.CharField(verbose_name=_('Teléfono'), max_length=15, blank=True, null=True)
    sexo_options = (
        (1, 'Hombre'),
        (2, 'Mujer'),

    )
    sexo = models.IntegerField(verbose_name=_('Sexo'), choices=sexo_options, blank=True, null=True)
    direccion = models.TextField(verbose_name=_('Dirección'), max_length=1000, blank=True, null=True)
    
    #Diccionario de formulario
    estado_options = (
        (1, 'aguascalientes'),
        (2, 'bajacalifornia'),
        (3, 'bajacaliforniasur'),
        (4, 'campeche'),
        (5, 'coahuila'),
        (6, 'chiapas'),
        (7, 'chiapas'),
        (8, 'chihuahua'),
        (9, 'distritofederal'),
        (10, 'durango'),
        (11, 'guanajuato'),
        (12, 'guerrero'),
        (13, 'hidalgo'),
        (14, 'jalisco'),
        (15, 'mexico'),
        (16, 'michoacan'),




        )

    estado = models.IntegerField(verbose_name=_('estado'), choices=estado_options, blank=True, null=True)
    codigo = models.IntegerField(verbose_name=('codigo'), blank=True, null=True)