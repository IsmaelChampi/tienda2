from django import forms
from usuarios.models import Perfil

from django.utils.translation import ugettext as _
class PerfilForm(forms.ModelForm):

    username = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre de usuario'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un usuario válido')},
        label=_('Nombre de usuario'),
        required=True,
    )

    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un nombre válido')},
        label=_('Nombre(s)*'),
        required=True,
    )

    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Apellidos(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un apellido válido')},
        label=_('Apellidos(s)*'),
        required=True,
    )


    direccion = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Dirección'), 'class': "form-control"}),
        max_length=200,
        error_messages={'required': _(u'Ingrese una dirección valida.')},
        label=_('Dirección'),
        required=True,
    )

    CHOICES= (
        ('1','Hombre'),
        ('2','Mujer'),
        )

    sexo = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion tu Sexo')},
        label=_('Sexo'),
        required=True,
    )

    codigo = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Codigo Postal'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese un codigo postal')},
        label=_('Codigo Postal'),
        required=True,
    )

    edad = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Edad'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese Edad ')},
        label=_('Edad'),
        required=True,
    )

    CHOICES= (
        ('1','aguascalientes'),
        ('2','bajacalifornia'),
        ('3','bajacaliforniasur'),
        ('4','campeche'),
        ('5','coahuila'),
        ('6','chiapas'),
        ('7','chiapas'),
        ('8','chihuahua'),
        ('9','distritofederal'),
        ('10','durango'),
        ('11','guanajuato'),
        ('12','guerrero'),
        ('13','hidalgo'),
        ('14','jalisco'),
        ('15','mexico'),
        ('16','michoacan'),
        )

    estado = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion tu Estado')},
        label=_('Estado'),
        required=True,
    )


    email = forms.EmailField(
        widget=forms.TextInput(attrs={'placeholder': _('Email(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese su Email')},
        label=_('Email'),
        required=True,
    )

    telefono = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Telefono'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese su Telefono ')},
        label=_('Telefono'),
        required=True,
    )




    class Meta:
        model = Perfil
        fields = ('username','first_name', 'last_name', 'direccion', 'sexo', 'codigo', 'edad', 'estado', 'email', 'telefono', )





        