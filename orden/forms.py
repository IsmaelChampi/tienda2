from django import forms
from orden.models import Order, Pago
from usuarios.models import Perfil
from productos.models import Cart
from django.utils.translation import ugettext as _




class ordenForm(forms.ModelForm):
    
    
        usuario = forms.ModelChoiceField(
        queryset=Perfil.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )


        codigopostal = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Codigopostal'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingresa codigo postal')},
        label=_('Codigopostal'),
        required=True,
    )    


        direccion = forms.CharField(
        widget=forms.TextInput( attrs={'class': "form-control"}),
    )


        CHOICES= (    
        ('1','aguascalientes'),
        ('2','bajacalifornia'),
        ('3','bajacaliforniasur'),
        ('4','campeche'),
        ('5','coahuila'),
        ('6','chiapas'),
        ('7','chiapas'),
        ('8','chihuahua'),
        ('9','distritofederal'),
        ('10','durango'),
        ('11','guanajuato'),
        ('12','guerrero'),
        ('13','hidalgo'),
        ('14','jalisco'),
        ('15','mexico'),
        ('16','michoacan'),

    )

        estado = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion tu Estado')},
        label=_('Estado'),
        required=True,
    )



        carrito = forms.ModelChoiceField(
        queryset=Cart.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

        impuesto = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Impuesto'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingresa el Impuesto')},
        label=_('Impuesto'),
        required=True,
    )

        subtotal = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Subtotal'), 'class': "form-control"}),
        label=_('Subtotal'),
        required=True,   
    )

        total = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Total'), 'class': "form-control"}),
        label=_('Total'),
        required=True,
    )

    

        class Meta:
            model = Order
            fields = ('usuario','codigopostal', 'carrito', 'impuesto', 'subtotal', 'total')



class pagoForm(forms.ModelForm):
    
    #Formulario productos

    usuario = forms.ModelChoiceField(
        queryset=Perfil.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    orden = forms.ModelChoiceField(
        queryset=Order.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

    CHOICES= (
        ('1','Efectivo'),
        ('2','Tarjeta'),

    )

    tipo_pago = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        error_messages={'required': _(u'Seleccion tu tipo de pago')},
        label=_('Tipo de Pago'),
        required=True,
    )

    monto = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Monto'), 'class': "form-control"}),
        label=_('Monto'),
        required=True,
    )

    CHOICES= (
        ('1', 'Exitoso'),
        ('2', 'Fallido'),
        ('3', 'Pendiente'),
        ('4', 'Cancelado'),

    )

    estatus = forms.ChoiceField(
        widget=forms.Select( attrs={'class': "form-control"}),
        choices=CHOICES,
        label=_('Estatus'),
        required=True,
    )

    



    class Meta:
        model = Pago
        fields = ('usuario', 'orden','tipo_pago', 'monto', 'estatus',)








