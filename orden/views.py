from django.shortcuts import render, redirect
from orden.forms import ordenForm, pagoForm
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.template.loader import get_template
# Create your views here.

from orden.models import Order
from orden.models import Pago
from productos.models import Cart, ProductCart

from django.http import HttpResponseRedirect
from django.urls import reverse

from django.views.decorators.csrf import csrf_exempt

import conekta
conekta.api_key = "key_fqQJYf2YdhySDa8EKJsGVw"



def add_orden(request):

    #revisar si existe cookie carrito, si si entonces:
    if request.COOKIES.get('carrito'):
        cookie = request.COOKIES.get('carrito')
        carrito = Cart.objects.get(pk=cookie)# si existe carrito nos da su id 
        print('cookie existente')
        #obtener productos carrito, 
        productCart = ProductCart.objects.filter(carrito=carrito)
        
        print(productCart)
        #iterar en los productos carrito para calcular el total de producto, 
        totalproductos = 0 
        for  produc in  productCart:
            #sumar total de cada producto en una variable subotal
            #sumar total de cada producto en una variable subotal
            #actual
            cantidad = produc.cantidad
            precio = produc.producto.price
            subtotal = int(cantidad) * int(precio)
            
            print(produc.producto.nombre_product)

            print('cantidad')
            print(cantidad)

            print('precio')
            print(precio)

            print('subtotal')
            print(subtotal)


            totalproductos=totalproductos+subtotal
            


            #del subtotal multiplicar por 0.16 que seria el impuesto
        
        print('totalproductos')
        print(totalproductos)
        impuesto = totalproductos * 0.16
        print('impuesto')
        print(impuesto)     


            #sumar subtotal + impuesto que sería el Total
        totaal = int(totalproductos) + int(impuesto)
        print('total global')
        print(totaal)


        return render(
            request=request,
            template_name='addorden.html',
            context={
                #'profile': profile,


                'productCart' : productCart,
                'impuesto' : impuesto,
                'totalproooductos' : totalproductos,
                'totaal' : totaal

            })


    return render(
        request=request,
        template_name='addorden.html',
        context={
            #'profile': profile,
   
            'productCart' : None,
            'impuesto' : 0,
            'totalproductos' : 0,
            'totaal' : 0
        })

def list_orden(request):

    ordenes = Order.objects.all()

    return render(
        request=request,
        template_name='list_orden.html',
        context={
            'ordenes': ordenes,
          
        })


def add_pago(request):
    if request.method == 'POST':
        #form = profileForm(request.POST, instance=request.user)  ##para actualizar un objeto en la base de datos
        form = pagoForm(request.POST)  #para crear un nuevo objeto en la base
        if form.is_valid():
            form.save()

            #new_user = Perfil.objects.create(first_name=data['nombrecompleto'], last_name=data['apellido'], direccion = data['direc'])

            return redirect('home')

    else:
        form = pagoForm() #para crer un nuevo objeto
        #form = profileForm(instance=request.user)   ##para actualizar un objeto en la base de datos


    return render(
        request=request,
        template_name='addpago.html',
        context={
            #'profile': profile,
            'form': form
        })

def list_pago(request):

    pagos = Pago.objects.all()

    return render(
        request=request,
        template_name='list_pago.html',
        context={
            'pagos': pagos,
          
        })


@csrf_exempt
def pago(request):

    usuario = request.user
    if request.method == 'POST':
        print('entro POST')
        if request.COOKIES.get('carrito'):
            cookie = request.COOKIES.get('carrito')
            print('existe carrito')
            carrito = Cart.objects.get(pk=cookie)# si existe carrito nos da su id 
            print('cookie existente')
            #obtener productos carrito, 
            productCart = ProductCart.objects.filter(carrito=carrito)
            print(productCart)
            #obtener stock de carrito

            #iterar en los productos carrito para calcular el total de producto, 
            totalproductos = 0 
            for  produc in  productCart:
            
                #stock
        
                #actualizar stock con cannew
               
                #sumar total de cada producto en una variable subotal
                #sumar total de cada producto en una variable subotal
                #actual
                cantidad = produc.cantidad
                precio = produc.producto.price

                subtotal = int(cantidad) * int(precio)
                
                print(produc.producto.nombre_product)

                print('cantidad')
                print(cantidad)

                print('precio')
                print(precio)

                print('subtotal')
                print(subtotal)


                totalproductos=totalproductos+subtotal

                #obtener el subtotal, impuestos y total de la orden

                #del subtotal multiplicar por 0.16 que seria el impuesto
            
            print('totalproductos')
            print(totalproductos)
            impuesto = totalproductos * 0.16
            print('impuesto')
            print(impuesto)     


                #sumar subtotal + impuesto que sería el Total
            totaal = int(totalproductos) + int(impuesto)
            print('total global')
            print(totaal)
            #crear un objeto orden con estos datos, pasando el carrito, 
            #el usuario y los datos de subtotal, impuestos y total

            print('direccion')
            print(request.POST)   

            direccion =  request.POST["direccion"]
            estado= int(request.POST["estado"])
            codigopostal = request.POST["codigopostal"]




            ordeen = Order.objects.create(carrito=carrito, direccion=direccion, estado=estado, codigopostal=codigopostal, usuario=usuario, total=totalproductos, impuesto=impuesto, subtotal=totaal)
            #crear un objeto Pago, con los datos del usuario, la orden que se creo arriba, 
            #status, tipo de pago y monto (total global)
            tipo_pago = 2
            estatus = 1

            try:
                #editar la consulta
                order = conekta.Order.create({
                "line_items": [{
                "name": "usuario",
                "unit_price": totalproductos,
                "quantity": 120
                }],
                "currency": "MXN",
                "customer_info": {
                "name": usuario.first_name,
                "phone": "+525533445566",
                "email": usuario.email,
                "corporate": False,      
                },
                "shipping_contact":{
                "address": {
                "street1": "Calle 123, int 2",
                "postal_code": "06100",
                "country": "MX"
                } #shipping_contact - required only for physical goods
                },
                "metadata": { "description": "Compra de creditos: 300(MXN)", "reference": "1334523452345" },
                "charges":[{
                "payment_method": {
                #"monthly_installments": 3,
                "type": "card",
                "token_id": request.POST["conektaTokenId"]      }  #payment_method - use the customer's default - a card
             #to charge a card, different from the default,
             #you can indicate the card's source_id as shown in the Retry Card Section
             }]
                })
                if order.payment_status == 'paid':#checamos el pago dentro de cpnecta 
                    pago = Pago.objects.create(usuario=usuario, orden=ordeen, tipo_pago=tipo_pago, estatus=estatus, monto=totaal)


                    subject = 'Asunto'
                    from_email = 'ismael.hernandez@upam.edu.mx'
                    to_email = [usuario.email]
                    contexto = {'productos':productCart, 'impuesto':impuesto, 'totalproductos':totalproductos, 'totaal':totaal }
                    message = get_template('correo.html').render(contexto)
                    msg = EmailMessage(subject, message, to=to_email, from_email=from_email)
                    msg.content_subtype = 'html'
                    msg.send()
                    #send_mail (
                    #'Subject here' ,
                    #'Gracias por tu compra.' ,
                    #html_message = render_to_string('correo.html'),
                    #'ismael.hernandez@upam.edu.mx' ,
                    #[ usuario.email ],
                    #fail_silently = False ,


                #)


            except conekta.ConektaError as e:
                print(e)

               

            response = render(
                request=request,
                template_name='pago.html',
                context={
                    'pago': 'pagoexitoso',
                    'productCart' : productCart,
                    'impuesto' : impuesto,
                    'totalproductos' : totalproductos,
                    'totaal' : totaal
                  
                })


            response.delete_cookie('carrito')
            return response


    return render(
                request=request,
                template_name='pago.html',
                context={
                    'pago': 'pagofallido',
                  
                })

#login view

    #... 
         
def login_view(request): 
    if request.user.is_authenticated: 
        return redirect('Cuenta') 
         
    if  request.method == 'POST': 
        username = request.POST.get('username') 
        password = request.POST.get('password') 
        productpk = request.POST.get('productpk')
        user = authenticate(username=username , password=password) 

        if user:
            print('si authenticate')
            login(request, user)
            if productpk:
                return HttpResponseRedirect(reverse('Detalle_product', args=(productpk)))
            else:
                return redirect('/')


        else:
            messages.error(request , 'Error wrong username/password')
            return redirect('login')

    return render(
                request=request, 
                template_name='login.html',
               )


         

def logout_view(request):
    logout(request)
    return redirect('login')
         
         
def  admin_page(request): 
    if not request.user.is_authenticated: 
        return redirect('') 
         
        return render(request,'base/admin_page.html') 












