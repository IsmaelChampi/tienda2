from django.contrib import admin
from orden.models import Order, Pago

# Register your models here.
@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('usuario','carrito', 'impuesto', 'subtotal')
    list_per_page = 30




@admin.register(Pago)
class PagoAdmin(admin.ModelAdmin):
    list_display = ('usuario','orden', 'tipo_pago', 'monto', 'estatus')
    list_per_page = 30