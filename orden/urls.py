from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

from django.urls import path


# View
from orden import views


urlpatterns = [

    # Posts
    path('addorden', views.add_orden, name='add_orden'),
    path('addpago', views.add_pago, name='add_pago'),
    path('listorden', views.list_orden, name='list_orden'),
    path('listpago', views.list_pago, name='list_pago'),
    path('Pago', views.pago, name='pago'),
    path('login', views.login_view, name='login'), 
    path('exit', views.logout_view, name='logout'), 
    path('admin_page', views.admin_page , name='admin_page'), 
     
    
    
    

]