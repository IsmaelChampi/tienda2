from django.db import models
from django.db import models
from django.db import models
from productos.models import ProductCart, Products
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
from django.dispatch import receiver
from woocommerce import API

class Order(models.Model):
    """ Modelo de orden de usuario """

    usuario = models.ForeignKey(
    'usuarios.Perfil',
    on_delete=models.CASCADE,
    )

    codigopostal = models.IntegerField(
    verbose_name=_('Codigopostal'), 
    blank=True, null=True)


    direccion = models.TextField(
    verbose_name=_('Dirección'), 
    max_length=1000, blank=True, null=True)

    estado_options = (
        (1, 'aguascalientes'),
        (2, 'bajacalifornia'),
        (3, 'bajacaliforniasur'),
        (4, 'campeche'),
        (5, 'coahuila'),
        (6, 'chiapas'),
        (7, 'chiapas'),
        (8, 'chihuahua'),
        (9, 'distritofederal'),
        (10, 'durango'),
        (11, 'guanajuato'),
        (12, 'guerrero'),
        (13, 'hidalgo'),
        (14, 'jalisco'),
        (15, 'mexico'),
        (16, 'michoacan'),
        )

    estado = models.IntegerField(
    verbose_name=_('Estado'), 
    choices=estado_options, 
    blank=True, null=True)


    carrito = models.ForeignKey(
    'productos.Cart',
    on_delete=models.CASCADE,
    )

    impuesto = models.IntegerField(
    verbose_name=_('Impuesto'), 
    blank=True, null=True)

    subtotal = models.IntegerField(
    verbose_name=_('Subtotal'), 
    blank=True, null=True)


    total = models.IntegerField(
    verbose_name=_('Total'), 
    blank=True, null=True)



    
  

class Pago(models.Model):
    """docstring for ClassName"""

    usuario = models.ForeignKey(
    'usuarios.Perfil',
    on_delete=models.CASCADE,
    )


    orden = models.ForeignKey(
    'orden.Order',
    on_delete=models.CASCADE,
    )

    tipo_pago_options = (
        (1, 'Efectivo'),
        (2, 'Tarjeta'),

        )

    tipo_pago = models.IntegerField(
    verbose_name=_('Tipopago'), 
    choices=tipo_pago_options, 
    blank=True, null=True)


    monto = models.IntegerField(
    verbose_name=_('Monto'), 
    blank=True, null=True)


    estatus_options = (
        (1, 'Exitoso'),
        (2, 'Fallido'),
        (3, 'Pendiente'),
        (4, 'Cancelado'),

        )

    estatus = models.IntegerField(
    verbose_name=_('Estatus'), 
    choices=estatus_options, 
    blank=True, null=True)


wcapi = API(
    url="https://tiendawordpress.tk",
    consumer_key="ck_7369a376ec329fdb3cce95ea0b7ad01d233f60c0",
    consumer_secret="cs_62eb3de235b86bcf771ccbfca6a015e3609ba6cd",
    version="wc/v3"
)

#api de woordpress
@receiver(post_save, sender=Order)
def order_stock(sender, instance, created, **kwargs):
    if created:
        
 
        cart = instance.carrito
        Productos = ProductCart.objects.filter(carrito=cart)
                
        for produ in Productos:

            currenstock = produ.producto.stock
            newstock = currenstock - produ.cantidad

            Products.objects.filter(pk=produ.producto.pk).update(stock=newstock)

            data = {
            "stock_quantity": newstock,
            } 
                    
            response = wcapi.put("products/"+str(produ.producto.woocomerid), data).json()
            print(response)


   






