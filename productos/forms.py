from django import forms
from productos.models import Products
from productos.models import Cart, ProductCart, ajax
from usuarios.models import Perfil

from django.utils.translation import ugettext as _
class productForm(forms.ModelForm):
    
    #Formulario productos

        nombre_product = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre de Producto(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese nombre de Producto')},
        label=_('Nombre de Producto'),
        required=True,
    )
        sku = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Codigo de Barra'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese su codigo de Barras')},
        label=_('Codigo de Barra'),
        required=True,
    )
        price = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Precio'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese su precio')},
        label=_('Precio'),
        required=True,
    )


        description = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder': _('Descrpcion'), 'class': "form-control"}),
        max_length=200,
        error_messages={'required': _(u'Ingrese Descrpcion.')},
        label=_('Descrpcion'),
        required=True,
    )

        stock = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Stock'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese stock')},
        label=_('Stock'),
        required=True,
    )

        class Meta:
            model = Products
            fields = ('nombre_product','sku', 'price','description', 'imagen', 'stock')
        

class cartForm(forms.ModelForm):
    
    #Formulario productos
        usuario = forms.ModelChoiceField(
        queryset=Perfil.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )


        class Meta:
            model = Cart
            fields = ('usuario',)




class cartproductForm(forms.ModelForm):
    
    #Agregar un carrito

        producto = forms.ModelChoiceField(
        queryset=Products.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),
    )

        cantidad = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Cantidad'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese su Cantidad')},
        label=_('Cantidad'),
        required=True,

    )
        carrito = forms.ModelChoiceField(
        queryset=Cart.objects.all(), 
        widget=forms.Select( attrs={'class': "form-control"}),

    )


        class Meta:
            model = ProductCart
            fields = ('producto', 'cantidad', 'carrito')



class ajaxForm(forms.ModelForm):
    
    #Agregar un carrito

        cantidad = forms.IntegerField(
        widget=forms.NumberInput(attrs={'placeholder': _('Cantidad'), 'class': "form-control"}),
        error_messages={'required': _(u'Ingrese su Cantidad')},
        label=_('Cantidad'),
        required=True,

    )

       




