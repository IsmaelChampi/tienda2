from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save, pre_delete

from django.dispatch import receiver


from usuarios.models import Perfil

from woocommerce import API

from store.settings import URL_SITIO
# Create your models productos.


#Modelo base de datos Productos
class Products(models.Model):
    """ Modelo de productos de usuario """
    nombre_product = models.TextField(
    verbose_name=('NombreProducto'), 
    max_length=100, 
    blank=True, null=True )

    imagen = models.ImageField(upload_to='images/', blank=True, null=True)


    sku = models.IntegerField(
    verbose_name=('Codigobarra'), 
    blank=True, null=True)
    

    price = models.IntegerField(
    verbose_name=('Precio'), 
    blank=True, null=True)


    description = models.TextField(
    verbose_name=('Descripcion'),
    blank=True)


    date = models.DateTimeField(auto_now_add=True)

    stock = models.IntegerField(
        verbose_name=('Stock'),
        blank=True)

    woocomerid = models.IntegerField(
        verbose_name=('Woocomerid'),
        blank=True, null=True)



#carrito usuario 
class Cart(models.Model):
    """ Modelo de carrito de usuario """
    usuario = models.ForeignKey(
        'usuarios.Perfil',
        on_delete=models.CASCADE,
    )

    fecha = models.DateTimeField(
    auto_now_add=True
    )


    total = models.IntegerField(
    verbose_name=('total'), 
    blank=True, null=True)


class ProductCart(models.Model):

    producto = models.ForeignKey(
       'Products',
       on_delete=models.CASCADE,
    )

    cantidad =  models.IntegerField(
    verbose_name=('Cantidad'), 
    blank=True, 
    null=True
    )

    carrito = models.ForeignKey(
        'Cart',
        on_delete=models.CASCADE,
    )
 

class ajax(models.Model):

    cantidad =  models.IntegerField(
    verbose_name=('Cantidad'), 
    blank=True, 
    null=True
    )

    idd =  models.IntegerField(
    verbose_name=('id'), 
    blank=True, 
    null=True
    )




wcapi = API(
    url="https://tiendawordpress.tk",
    consumer_key="ck_7369a376ec329fdb3cce95ea0b7ad01d233f60c0",
    consumer_secret="cs_62eb3de235b86bcf771ccbfca6a015e3609ba6cd",
    version="wc/v3"
)





@receiver(post_save, sender=Products)
def update_stock(sender, instance, created, **kwargs):
    if created:
        
        data = {
        "name": instance.nombre_product,
        "regular_price": str(instance.price),
        "sku": str(instance.sku),
        "description": instance.description,
        "stock_quantity": instance.stock,
        "manage_stock": True,
        "meta_data": [
            {
            'key':'djangoid',
            'value': instance.pk #obtener id de django con woordpress
            }
        ],
            "images": [
        {
           "src": URL_SITIO+instance.imagen.url
            #"src": "https://storedjango.tk/media/images/chaleco2.jpg.400x300_q85_crop.jpg"
        },
        ]
            }

        response = wcapi.post("products", data).json()

        print(response)

        try:
            print(response['id'])
            idwoo = response['id']
            Products.objects.filter(pk = instance.pk).update(woocomerid=response['id'])

        except:
            #TODO borrar
            pass


    else:

        data = {
        "name": instance.nombre_product,
        "regular_price": str(instance.price),
        "description": instance.description,
        "stock_quantity": instance.stock,
              "images": [
        {
           "src": URL_SITIO+instance.imagen.url
            #"src": "https://storedjango.tk/media/images/chaleco2.jpg.400x300_q85_crop.jpg"
        },
        ]
        }

        response= wcapi.put("products/"+str(instance.woocomerid),data).json()
        #print(response)



@receiver(pre_delete, sender=Products)
def delete_product(sender, instance, **kwargs):
  
    response = wcapi.delete("products/"+str(instance.woocomerid)+"?force=true").json()

    print(response)

  
















    

    

    








