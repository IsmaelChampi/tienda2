from django.contrib import admin

from productos.models import Products
from productos.models import Cart, ProductCart
# Register your models here.


@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    list_display = ('nombre_product', 'sku')
    list_per_page = 30



@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ('usuario',)
    list_per_page = 30




@admin.register(ProductCart)
class CartProductAdmin(admin.ModelAdmin):
    list_display = ('producto',)
    list_per_page = 30

