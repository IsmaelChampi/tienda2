from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include


# View
from productos import views


#url del modelo-vista
urlpatterns = [

    path('', views.home_product, name='home'),
    path('new', views.products_new, name='products_new'),
    path('list', views.list_product, name='list_product'),
 	path('detalle/<int:pk>/', views.Detalle_product, name='Detalle_product'),
 	path('cart', views.NewCart, name='Cart'),
 	path('cartlist', views.list_cart, name='list_cart'),
 	path('cartadd', views.add_cart, name='add_cart'),
   	path('ajax/', views.ajaxproduct), 
   	path('ajaxload/', views.loadproduct),
   	path('delete/', views.delete_product),
   	path('ajaxupdate/', views.ajaxupdate),
   	
   	


]