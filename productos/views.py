from django.shortcuts import render, redirect

from easy_thumbnails.files import get_thumbnailer

from productos.forms import productForm, cartForm, cartproductForm


from productos.models import Products, Cart, ProductCart

from django.views.decorators.csrf import csrf_exempt


from django.http import HttpResponse, JsonResponse
import json




# Creacion vista product_profile para ingresar datos
def products_new(request):
    if request.method == 'POST':
        #form = profileForm(request.POST, instance=request.user)  ##para actualizar un objeto en la base de datos
        form = productForm(request.POST, request.FILES)  #para crear un nuevo objeto en la base
        if form.is_valid():
            form.save()

            #new_user = Perfil.objects.create(first_name=data['nombrecompleto'], last_name=data['apellido'], direccion = data['direc'])

            return redirect('home')

    else:
        form = productForm() #para crer un nuevo objeto
        #form = profileForm(instance=request.user)   ##para actualizar un objeto en la base de datos


    return render(
        request=request,
        template_name='products_new.html',
        context={
            #'profile': profile,
            'form': form
        })





#vista home 
def home_product(request):
    products = Products.objects.all()

    return render(
        request=request,
        template_name='home.html',
        context={
            'products': products,
          
        })

    



#listar productos en el template
def list_product(request):

    products = Products.objects.all()

    return render(
        request=request,
        template_name='list_product.html',
        context={
            'products': products,
          
        })


#detalles de producto en el template
def Detalle_product(request, pk):

    usuarioid = request.user.pk

    product = Products.objects.get(pk=pk)


    return render(
        request=request,
        template_name='Detalle_product.html',
        context={
            'product': product,
            'usuarioid': usuarioid,
          
        })

@csrf_exempt
def ajaxproduct(request):
    data = request.body
    data = json.loads(data)

    print('ajax product')
    print (data)
    cantidad = (data['cantidad'])
    productoid = (data['idproducto'])

    print('producto id')
    print(productoid)

    
    producto = Products.objects.get(id=productoid) #sera la relacion del id de producto

    print(producto)
    
    #si existe carrito
    if request.COOKIES.get('carrito'):

        print('cookie existente')
        cookie = request.COOKIES.get('carrito')
        carrito = Cart.objects.get(pk=cookie)# si existe carrito nos da su id

        print('carrito')
        print(carrito.pk)


        productCart = ProductCart.objects.filter(producto=producto, carrito=carrito).count()

        #se suma la cantidad almacenada + la actual

        if productCart > 0:

            #cantidad almacenada
            actual = ProductCart.objects.get(producto=producto, carrito=carrito)
            #cantidad actual
            canactual = actual.cantidad
            cannew = int(cantidad) + canactual


             #stock
            actstoc = producto.stock
            canstocnew = actstoc - cannew

            if not canstocnew >= 0:

                print('no hay stock');

                response = JsonResponse({'status': 'nostock'})
                return response 

            else:
                print("si hay suficiente stock")


                #obterner 

                #actualizar ProductCart con cannew
                ProductCart.objects.filter(producto=producto, carrito=carrito).update(cantidad=cannew)

                productCart = ProductCart.objects.filter(producto=producto, carrito=carrito)[0]

                options = {'size': (100, 100), 'crop': True}
                thumb_url = get_thumbnailer(productCart.producto.imagen).get_thumbnail(options).url


                print('cantidad')

                print(productCart.id)

                response = JsonResponse({'status': 'success', 'idproducto': productCart.id, 'nombre':productCart.producto.nombre_product, 'precio':productCart.producto.price,'cantidad': productCart.cantidad, 'imagen':thumb_url})
                return response   
            

        else:
            productCart = ProductCart.objects.create(cantidad=cantidad, producto=producto, carrito=carrito)


             #cantidad almacenada
            actual = ProductCart.objects.get(producto=producto, carrito=carrito)
            #cantidad actual
            canactual = actual.cantidad
            cannew = int(cantidad) + canactual


             #stock
            actstoc = producto.stock
            canstocnew = actstoc - cannew

            if not canstocnew >= 0:

                print('no hay stock');

                response = JsonResponse({'status': 'nostock'})
                return response 

            else:
                print("si hay suficiente stock")

                print('opcion1')
                print(productCart)
                response = JsonResponse({'status': 'success', 'idproducto': productCart.id, 'nombre':productCart.producto.nombre_product, 'precio':productCart.producto.price,'cantidad': productCart.cantidad, 'imagen':productCart.producto.imagen.url})
                return response   

    else: #si no exite carrito se crea


        carrito = Cart.objects.create(usuario=request.user)

        print('carrito')
        print(carrito.pk)


        productCart = ProductCart.objects.create(cantidad=cantidad, producto=producto, carrito=carrito)

         #cantidad almacenada
        actual = ProductCart.objects.get(producto=producto, carrito=carrito)
        #cantidad actual
        canactual = actual.cantidad
        cannew = int(cantidad) + canactual


         #stock
        actstoc = producto.stock
        canstocnew = actstoc - cannew

        if not canstocnew >= 0:

            print('no hay stock');

            response = JsonResponse({'status': 'nostock'})
            return response 

        else:
            print("si hay suficiente stock")

            print('cooki no existe')

            print('opcion2')
            print(productCart)
            response = JsonResponse({'status': 'success','idproducto': productCart.id, 'nombre':productCart.producto.nombre_product, 'precio':productCart.producto.price,'cantidad': productCart.cantidad, 'imagen':productCart.producto.imagen.url})
            response.set_cookie(key='carrito', value=carrito.pk)
            return response

    



@csrf_exempt
def ajaxupdate(request):
    data = request.body
    data = json.loads(data)

    print('ajax product')
    print (data)
    cantidad = (data['cantidad'])
    productoid = (data['idproducto'])
    

    #si existe carrito
    if request.COOKIES.get('carrito'):
        productCart = ProductCart.objects.filter(id=productoid).count()

        carrito = int(request.COOKIES.get('carrito'))
        print(carrito)

        #se suma la cantidad almacenada + la actual
     
        if productCart > 0:

            cannew = int(cantidad)
            #actualizar ProductCart con cannew
            ProductCart.objects.filter(id=productoid).update(cantidad=cannew)
            productCart = ProductCart.objects.filter(id=productoid)[0]


              #stock almacenado
            actstoc = productCart.producto.stock
            canstocnew = actstoc - cannew
            if not canstocnew >= 0:

                print("no hay stock")

                response = JsonResponse({'status': 'nostock'})
                return response 

            else:
                print("si hay suficiente stock")


                productscart = ProductCart.objects.filter(carrito_id=carrito)

                options = {'size': (100, 100), 'crop': True}



                

                totalproooductos = 0 
                for  produc in   productscart:
                    cantidad = produc.cantidad
                    precio = produc.producto.price
                    thumb_url = get_thumbnailer(produc.producto.imagen).get_thumbnail(options).url


                    #subtotal
                    sub = int(cantidad) * int(precio)
                    print('sub')
                    print(sub)

                    #total global
                    totalproooductos=totalproooductos+sub
                    print('totalproooductos')
                    print(totalproooductos)

                impuesto = totalproooductos  * 0.16
                print('impuesto')
                print(impuesto)     


             #sumar subtotal + impuesto que sería el Total
                totaal = int(totalproooductos ) + int(impuesto)
                print('total global')
                print(totaal)


             #subtotal = precio * cantidad
                print('cantidad')
                print(productCart.id)
                
            
                print('opcion1')
                print(productCart)
                response = JsonResponse({'status': 'success', 'idproducto': productCart.id, 'nombre':productCart.producto.nombre_product, 'precio':productCart.producto.price,'cantidad': productCart.cantidad,  'imagen': thumb_url, 'impuesto':impuesto, 'totalproooductos':totalproooductos, 'totaal':totaal})
                return response   





@csrf_exempt
def loadproduct(request):
    #si es existe carrito)
    
    listaproductcart = []

    if request.COOKIES.get('carrito'):
        cookie = request.COOKIES.get('carrito')
        carrito = Cart.objects.get(pk=cookie)# si existe carrito nos da su id 
        print('cookie existente')
        productCart = ProductCart.objects.filter(carrito=carrito)
        options = {'size': (100, 100), 'crop': True}
        
        print(productCart)

        #dicionario vacio

        #agregar en cada iteracion el diccionario creado a la lista listaproductcart
        
        #en un ciclo for iterar en cada elemento de productCart
        for  produ in  productCart:
            print(produ)
            thumb_url = get_thumbnailer(produ.producto.imagen).get_thumbnail(options).url

            #crear un diccionario con los datos del producto, idproducto, nombre, precio y cantidad
            productdic = {
                'idproducto': produ.id, 
                'nombre': produ.producto.nombre_product,
                'precio': produ.producto.price,
                'cantidad': produ.cantidad,
                'imagen':  thumb_url
                #'subtotal':  produ.producto.price * produ.cantidad
                }

            #metodo append de agregar diccionario
            listaproductcart.append(productdic)
            



        #convertir diccionario en json
    print(listaproductcart)
    response = json.dumps(listaproductcart,  ensure_ascii=False)
    print(response)
    return JsonResponse(response, safe=False)


def NewCart(request):
    if request.method == 'POST':
        form = cartForm(request.POST) 
        if form.is_valid():
            form.save()

            

            return redirect('home')

    else:
        form = cartForm()


    return render(
        request=request,
        template_name='cart.html',
        context={
            #'profile': profile,
            'form': form
        })


def list_cart(request):
    carts = Cart.objects.all()
    variable1 = 'hola'

    return render(
        request=request,
        template_name='list_card.html',
        context={
            'Carts1': carts,
            'myvariable': variable1 
          
        })



def add_cart(request):
    if request.method == 'POST':
        #form = profileForm(request.POST, instance=request.user)  ##para actualizar un objeto en la base de datos
        form = cartproductForm(request.POST)  #para crear un nuevo objeto en la base
        if form.is_valid():
            form.save()

            #new_user = Perfil.objects.create(first_name=data['nombrecompleto'], last_name=data['apellido'], direccion = data['direc'])

            return redirect('home')

    else:
        form = cartproductForm() #para crer un nuevo objeto
        #form = profileForm(instance=request.user)   ##para actualizar un objeto en la base de datos


    return render(
        request=request,
        template_name='addcart.html',
        context={
            #'profile': profile,
            'form': form
        })


@csrf_exempt
#eliminar producto
def delete_product(request):
    if request.method == 'POST':
        data = request.body
        data = json.loads(data)

        print('ajax product')
        print (data)
        productoid = (data['idproducto'])
        ProductCart.objects.filter(id=productoid).delete()  
        response = JsonResponse({'idproducto': productoid})
        return response








































	
   