from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

from django.urls import path


from rest_framework.authtoken import views
from api.products_view import productnew, updatestok, List_produ


from api.auth_views import CustomAuthToken



urlpatterns = [

   path('api-token-auth/', views.obtain_auth_token),
   path('api-token-auth-2/', CustomAuthToken.as_view()),
   path('add_product/', productnew),
   path('update_stok/<int:pk>/', updatestok),
   path('list_product/', List_produ),
]  