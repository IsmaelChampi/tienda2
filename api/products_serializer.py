#Product serializer
#Django Rest Framework
from rest_framework import serializers 


class Productserializer(serializers.Serializer):
	#product serializer
	nombre_product = serializers.CharField()
	sku = serializers.IntegerField()
	price = serializers.IntegerField()
	description = serializers.CharField()
	stock = serializers.IntegerField()
	#imagen = serializers.ImageField()
	woocomerid = serializers.IntegerField()





class stockserializer(serializers.Serializer):
	#stock serializers
	stock = serializers.IntegerField()

	def update(self, instance, validated_data):
		instance.stock = validated_data.get('stock', instance.stock)
		#obtener stock actual del producto y restar el stock de compra 
		product_stock = self.context['stock_product']
		print('stock actual')
		print(product_stock)
		new_stock = product_stock-instance.stock
		# nuevo stock (resultado de operacion), es el que se debe guardar
		instance.stock = new_stock 
		instance.save()
		return instance

 
class Listproserializer(serializers.Serializer):
	#lista de prodcutos 
	nombre_product = serializers.CharField()
	sku = serializers.IntegerField()
	price = serializers.IntegerField()
	description = serializers.CharField()
	stock = serializers.IntegerField()
