
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes 
from rest_framework.authentication import TokenAuthentication
#Models
from productos.models import Products
#serializer 
from api.products_serializer import Productserializer, stockserializer, Listproserializer
#from serializers import stockserializer


#vista para dar de alta producto
@authentication_classes((TokenAuthentication))
@api_view(['POST'])
def productnew(request):
	#Dar de alta products
	serializers = Productserializer(data=request.data)
	serializers.is_valid(raise_exception=True)
	data = serializers.data
	print(data)
	producto = Products.objects.create(**data)
	return Response(Productserializer(producto).data)


#vista para actualizar stock de producto
@authentication_classes((TokenAuthentication))
@api_view(['POST'])
def updatestok(request, pk):
	try:
		product = Products.objects.get(pk=pk)
	except Products.DoesNotExist: 
		return Response(status=status.HTTP_404_NOT_FOUND)

	print(request.data)
	print('current stock')
	print(product.stock)

	serializers = stockserializer(product, data=request.data, context={'stock_product': product.stock})
	serializers.is_valid()
	serializers.save()
	return Response({"message": "Producto actualizado"})

@authentication_classes((TokenAuthentication))
@api_view(['GET'])
def List_produ(request):
	#lista de productos de Products
	productos = Products.objects.all()
	data = []
	for produ in productos:
		serializer = Listproserializer(produ)
		data.append(serializer.data)
	return Response(data)



